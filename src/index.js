const mensajes = [
    "Los ordenadores son inútiles. Sólo pueden darte respuestas",
    "Hardware: las partes de un ordenador que pueden ser pateadas",
    "Primero resuelve el problema. Entonces, escribe el código",
    "El buen código es su mejor documentación",
    "Los buenos programadores usan sus cerebros, pero unas buenas directrices nos ahorran de tener que hacerlo en cada caso",
    "Depurar es al menos dos veces más duro que escribir el código por primera vez",
    "¡No me importa si funciona en tu máquina! ¡No estamos vendiendo tu máquina!",
    "Para crecer, necesitas traicionar las expectativas de los demás",
    "La creatividad es la inteligencia divirtiéndose",
    "Toma riesgos y prepárate para fallar.",
    
  ];
  
  const miDependencia = () => {
    const mesaje = mensajes[Math.floor(Math.random() * mensajes.length)];
    console.log(`\x1b[34m${mesaje}\x1b[89m`);
  }
  
  module.exports = {
    miDependencia
  };
  